/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import DataType from 'sequelize';
import Model from '../sequelize';

const Lab = Model.define(
  'Lab',
  {
    id: {
      type: DataType.UUID,
      defaultValue: DataType.UUIDV1,
      primaryKey: true,
    },
    labName: {
      type: DataType.STRING(100),
      validate: { notEmpty: true },
      defaultValue: 'No Name',
    },
    labValue: {
      type: DataType.STRING(100),
      validate: { notEmpty: true },
      defaultValue: '0',
    },
    labDate: {
      type: DataType.DATEONLY,
      validate: { notEmpty: true },
    },

  },
);

export default Lab;
