/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable linebreak-style */
/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */
import sequelize from 'sequelize';
import express from 'express';
import Lab from './data/models/Lab';

const eRouter = express.Router();

eRouter.get('/recentlabs', (req, res) => {
  Lab.findAll({
    group: ['labName'],
    order: [sequelize.fn('MAX', sequelize.col('labDate'))],
  })
    .then(labs => {
      res.json({
        success: true,
        msgCode: 'SUCCESS_GET_LABS',
        labs,
      });
    })
    .catch(error => {
      res.json({
        success: false,
        msgCode: 'ERR_GET_LABS_LIST',
        msgText: error,
        lab: {},
      });
    });
});

eRouter.get('/lab', (req, res) => {
  Lab.all({ order: [['labDate', 'DESC']] })
    .then(labs => {
      res.json({
        success: true,
        msgCode: 'SUCCESS_GET_LABS',
        labs,
      });
    })
    .catch(error => {
      res.json({
        success: false,
        msgCode: 'ERR_GET_LABS_LIST',
        msgText: error,
        lab: {},
      });
    });
});

// eslint-disable-next-line no-unused-vars
eRouter.post('/lab', (req, res, next) => {
  // eslint-disable-next-line no-console
  let success = true;
  let msgCode = true;

  if (!req.body.labName.length) {
    success = false;
    msgCode = 'ERR_LAB_NAME_MISSING';
  }
  if (!req.body.labValue.length) {
    success = false;
    msgCode = 'ERR_LAB_VALUE_MISSING';
  }
  if (!req.body.labDate) {
    success = false;
    msgCode = 'ERR_LAB_DATE_MISSING';
  }

  if (!success) {
    res.json({
      success: false,
      msgCode,
      lab: {},
    });

    return;
  }

  const lab = Lab.build({
    labName: req.body.labName,
    labValue: req.body.labValue,
    labDate: req.body.labDate,
  });

  lab.save().catch(error => {
    res.json({
      success: false,
      msgCode: 'ERR_ADD_LAB',
      msgText: error,
      lab: {},
    });
  });

  res.json({
    success: true,
    msgCode: 'SUCCESS_ADD_LAB',
    lab,
  });
});

// eslint-disable-next-line no-unused-vars
eRouter.put('/lab/:labId', (req, res, next) => {
  // eslint-disable-next-line no-console

  let success = true;
  let msgCode = true;

  if (!req.body.labId.length) {
    success = false;
    msgCode = 'ERR_LAB_ID_MISSING';
  }
  if (!req.body.labName.length) {
    success = false;
    msgCode = 'ERR_LAB_NAME_MISSING';
  }
  if (!req.body.labValue.length) {
    success = false;
    msgCode = 'ERR_LAB_VALUE_MISSING';
  }
  if (!req.body.labDate) {
    success = false;
    msgCode = 'ERR_LAB_DATE_MISSING';
  }

  if (!success) {
    res.json({
      success: false,
      msgCode,
      lab: {},
    });

    return;
  }

  Lab.update(
    {
      labName: req.body.labName,
      labValue: req.body.labValue,
      labDate: req.body.labDate,
    },
    { where: { id: req.body.labId } },
  )
    .then(result => {
      res.json({
        success: true,
        msgCode: 'SUCCESS_UPDATE_LAB',
        lab: result,
      });
    })
    .catch(err => {
      res.json({
        success: false,
        msgCode: 'ERR_ADD_LAB',
        msgText: err,
        lab: {},
      });
    });
});

eRouter.delete('/lab/:labId', (req, res) => {
  let success = true;
  let msgCode = true;

  if (!req.body.labId.length) {
    success = false;
    msgCode = 'ERR_LAB_ID_MISSING';
  }

  if (!success) {
    res.json({
      success: false,
      msgCode,
      lab: {},
    });

    return;
  }

  Lab.destroy({ where: { id: req.body.labId } })
    .then(() => {
      res.json({
        success: true,
        msgCode: 'SUCCESS_DELETE_LAB',
      });
    })
    .catch(err => {
      res.json({
        success: false,
        msgCode: 'ERR_DELETE_LAB',
        msgText: err,
      });
    });
});

export default eRouter;
