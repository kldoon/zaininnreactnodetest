/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './AllLabs.css';
// eslint-disable-next-line import/no-named-as-default
import SimpleTable from '../../components/SimpleTable/SimpleTable';

class AllLabs extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      rows: [],
    };

    this.getAllLabs = this.getAllLabs.bind(this);
    this.handelUpdates = this.handelUpdates.bind(this);
  }

  componentDidMount() {
    this.getAllLabs();
  }

  getAllLabs = async () => {
    await fetch('/api/lab', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(res => {
        if (res && res.success) {
          this.setState({ rows: res.labs || [] });
        } else {
          // TODO: Handle Error
        }
      });
  };

  handelUpdates() {
    this.getAllLabs();
  }

  render() {
    return (
      <div className={s.root}>
        <div className={s.container}>
          <h2>
            {this.props.title} <span>({this.state.rows.length})</span>{' '}
          </h2>
          <SimpleTable
            rows={this.state.rows}
            handelUpdates={this.handelUpdates}
            showActions
          />
        </div>
      </div>
    );
  }
}

export default withStyles(s)(AllLabs);
