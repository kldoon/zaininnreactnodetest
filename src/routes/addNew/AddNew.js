/* eslint-disable react/prop-types */
/* eslint-disable react/no-unused-state */
/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import cx from 'classnames';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import s from './AddNew.css';

class AddNew extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      labName: '',
      labValue: '',
      labDate: '',
      successAdd: null,
      addMsg: '',
    };

    this.addLab = this.addLab.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.clearForm = this.clearForm.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  clearForm() {
    document.getElementById('newLabFrm').reset();
    this.setState({ labName: '' });
    this.setState({ labValue: '' });
    this.setState({ labDate: '' });
    this.setState({ successAdd: null });
    this.setState({ addMsg: '' });
  }

  saveLab = async data => {
    await fetch('/api/lab', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(res => {
        if (res && res.success) {
          this.setState({ successAdd: true });
          this.setState({ addMsg: 'Lab Added Successfully' });
        } else {
          this.setState({ successAdd: false });
          this.setState({ addMsg: 'Failed to Add Lab!' });
        }
      });
  };

  addLab(event) {
    event.preventDefault();
    const data = this.state;
    this.saveLab(data);
  }

  render() {
    return (
      <div className={s.root}>
        <div className={s.container}>
          <h2>{this.props.title}</h2>
          <div className={s.textDesc}>Please Enter the Lab Details:</div>
          <form onSubmit={this.addLab} id="newLabFrm">
            <div className={s.formGroup}>
              <TextField
                id="labName"
                name="labName"
                label="Lab Name"
                className={this.props.textField}
                onChange={this.handleInputChange}
                type="text"
                margin="normal"
                variant="filled"
                fullWidth
                required
              />
            </div>
            <div className={s.formGroup}>
              <TextField
                id="labValue"
                name="labValue"
                label="Lab Value"
                className={this.props.textField}
                onChange={this.handleInputChange}
                type="text"
                margin="normal"
                variant="filled"
                fullWidth
                required
              />
            </div>
            <div className={s.formGroup}>
              <TextField
                id="labDate"
                name="labDate"
                label="Lab Date"
                type="date"
                className={this.props.textField}
                onChange={this.handleInputChange}
                pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}"
                InputLabelProps={{
                  shrink: true,
                }}
                fullWidth
                required
              />
            </div>
            <div
              className={cx(
                s.formGroup,
                s.formMessage,
                this.state.successAdd ? s.successMsg : s.errorMsg,
              )}
            >
              {this.state.addMsg}
            </div>
            <div className={s.formGroup}>
              <Button
                className={s.save}
                variant="contained"
                color="primary"
                type="submit"
              >
                Save
              </Button>
              <Button
                className={s.clear}
                variant="contained"
                onClick={this.clearForm}
              >
                Clear
              </Button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(AddNew);
