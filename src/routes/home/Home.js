/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Home.css';
import SimpleTable from '../../components/SimpleTable/SimpleTable';

class Home extends React.Component {
  static propTypes = {};

  constructor(props) {
    super(props);

    this.state = {
      rows: [],
    };

    this.getAllLabs = this.getAllLabs.bind(this);
  }

  componentDidMount() {
    this.getAllLabs();
  }

  getAllLabs = async () => {
    await fetch('/api/recentlabs', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(res => {
        if (res && res.success) {
          this.setState({ rows: res.labs || [] });
        } else {
          // TODO: Handle Error
        }
      });
  };

  handelUpdates() {
    this.getAllLabs();
  }

  render() {
    return (
      <div className={s.root}>
        <div className={s.container}>
          <h2>Dashboard</h2>
          <div className={s.textDesc}>Recently Added Labs:</div>
          <SimpleTable
            rows={this.state.rows}
            handelUpdates={this.handelUpdates}
            showActions={false}
          />
        </div>
      </div>
    );
  }
}

export default withStyles(s)(Home);
