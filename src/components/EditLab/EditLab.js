import React from 'react';
// eslint-disable-next-line no-unused-vars
import PropTypes, { func } from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Tooltip from '@material-ui/core/Tooltip';
import cx from 'classnames';
import s from './EditLab.css';

export class EditLab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      openDelete: false,
      labName: '',
      labValue: '',
      labDate: '',
      labId: '',
      addMsg: '',
      successAdd: '',
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.saveLab = this.saveLab.bind(this);
    this.deleteLab = this.deleteLab.bind(this);
  }

  handleInputChange(event) {
    // eslint-disable-next-line prefer-destructuring
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    // eslint-disable-next-line prefer-destructuring
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  handleClose = () => {
    this.setState({ open: false });
    this.setState({ openDelete: false });
  };

  handleSaveEdit = () => {
    const lab = {
      labName: this.state.labName,
      labValue: this.state.labValue,
      labDate: this.state.labDate,
      labId: this.state.labId,
    };
    this.saveLab(lab);
  };

  clearState = () => {
    this.setState({ labName: this.props.lab.labName });
    this.setState({ labValue: this.props.lab.labValue });
    this.setState({ labDate: this.props.lab.labDate });
    this.setState({ labId: this.props.lab.id });
    this.setState({ openDelete: false });
    this.setState({ open: false });
    this.setState({ addMsg: '' });
    this.setState({ successAdd: '' });
  };

  handleEdit = () => {
    this.clearState();
    if (this.props.lab) {
      this.setState({ labName: this.props.lab.labName });
      this.setState({ labValue: this.props.lab.labValue });
      this.setState({ labDate: this.props.lab.labDate });
      this.setState({ labId: this.props.lab.id });
      this.setState({ open: true });
    }
  };

  handleDelete = () => {
    this.clearState();
    if (this.props.lab) {
      this.setState({ labName: this.props.lab.labName });
      this.setState({ labValue: this.props.lab.labValue });
      this.setState({ labDate: this.props.lab.labDate });
      this.setState({ labId: this.props.lab.id });
      this.setState({ openDelete: true });
    }
  };

  deleteLab = async () => {
    const data = {
      labId: this.state.labId,
    };

    await fetch(`/api/lab/${data.labId}`, {
      method: 'DELETE',
      body: JSON.stringify(data),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(res => {
        if (res && res.success) {
          this.setState({ openDelete: false });
          this.props.handelUpdates('val');
        } else {
          // this.setState({ successAdd: false });
          this.setState({ addMsg: 'Failed to Delete Lab!' });
        }
      });
  };

  saveLab = async data => {
    await fetch(`/api/lab/${data.labId}`, {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(res => {
        if (res && res.success) {
          this.setState({ open: false });
          this.props.handelUpdates('val');
        } else {
          this.setState({ successAdd: false });
          this.setState({ addMsg: 'Failed to Update Lab!' });
        }
      });
  };
  render() {
    return (
      <span>
        <Tooltip title="Edit" placement="top">
          <IconButton aria-label="Edit" onClick={this.handleEdit}>
            <EditIcon fontSize="small" color="primary" />
          </IconButton>
        </Tooltip>
        <Tooltip title="Delete" placement="top">
          <IconButton aria-label="Delete" onClick={this.handleDelete}>
            <DeleteIcon fontSize="small" color="secondary" />
          </IconButton>
        </Tooltip>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <form>
            <DialogTitle id="form-dialog-title">Edit Lab</DialogTitle>
            <DialogContent>
              <DialogContentText>
                Please edit the lab details then click save to keep the new
                edits.
              </DialogContentText>
              <div>
                <TextField
                  id="labName"
                  name="labName"
                  label="Lab Name"
                  value={this.state.labName}
                  onChange={this.handleInputChange}
                  type="text"
                  margin="normal"
                  variant="filled"
                  fullWidth
                  required
                />
              </div>
              <div>
                <TextField
                  id="labValue"
                  name="labValue"
                  label="Lab Value"
                  value={this.state.labValue}
                  onChange={this.handleInputChange}
                  type="text"
                  margin="normal"
                  variant="filled"
                  fullWidth
                  required
                />
              </div>
              <div>
                <TextField
                  id="labDate"
                  name="labDate"
                  label="Lab Date"
                  type="date"
                  value={this.state.labDate}
                  onChange={this.handleInputChange}
                  pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  fullWidth
                  required
                />
              </div>
              <div
                className={cx(
                  s.formMessage,
                  this.state.successAdd ? s.successMsg : s.errorMsg,
                )}
              >
                {this.state.addMsg}
              </div>
              <div />
            </DialogContent>
            <DialogActions>
              <Button
                className={s.clear}
                variant="contained"
                onClick={this.handleClose}
              >
                Cancel
              </Button>
              <Button
                className={s.save}
                variant="contained"
                onClick={this.handleSaveEdit}
                color="primary"
              >
                Save
              </Button>
            </DialogActions>
          </form>
        </Dialog>
        <Dialog
          open={this.state.openDelete}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            Do You want to delete this lab ({this.props.lab.labName})?
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              The lab will be permanently deleted and you will not be able to
              restore it!
            </DialogContentText>
            <div
              className={cx(
                s.formMessage,
                this.state.successAdd ? s.successMsg : s.errorMsg,
              )}
            >
              {this.state.addMsg}
            </div>
          </DialogContent>
          <DialogActions>
            <Button
              className={s.clear}
              variant="contained"
              onClick={this.handleClose}
            >
              Cancel
            </Button>
            <Button
              className={s.save}
              variant="contained"
              onClick={this.deleteLab}
              color="secondary"
            >
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </span>
    );
  }
}

EditLab.propTypes = {
  handelUpdates: PropTypes.func.isRequired,
  lab: PropTypes.shape({
    labName: PropTypes.string.isRequired,
    labValue: PropTypes.string.isRequired,
    labDate: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
  }).isRequired,
};

export default withStyles(s)(EditLab);
