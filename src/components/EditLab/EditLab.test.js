/* eslint-disable no-undef */
import React from 'react';
import Enzyme, { shallow, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { EditLab } from './EditLab';
// import { NavTabs } from '../NavTabs/NavTabs';

Enzyme.configure({ adapter: new Adapter() });

const row = {
  labName: 'testtest',
  labDate: '11/25/2018',
  labValue: '2.5',
  id: '2654654564',
};

function handelUpdates(obj) {
  // eslint-disable-next-line no-console
  console.log(obj);
}
describe('EditLab', () => {
  it('should have an Edit icon button and Delete icon button', () => {
    const wrapper = shallow(
      <EditLab lab={row} handelUpdates={handelUpdates} />,
    );

    // console.log(wrapper.debug());
    expect(wrapper.find('pure(DeleteIcon)').length).toBe(1);
    expect(wrapper.find('pure(EditIcon)').length).toBe(1);
  });
});
