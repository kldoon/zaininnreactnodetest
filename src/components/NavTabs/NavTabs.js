/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/prefer-stateless-function */
/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import cx from 'classnames';
// eslint-disable-next-line css-modules/no-unused-class
import s from './NavTabs.css';
import Link from '../Link';
import history from '../../history';

export class NavTabs extends React.Component {
  render() {
    let currentLocation = '/';
    if (history && history.location && history.location.pathname) {
      currentLocation = history.location.pathname;
    }

    return (
      <div className={s.root}>
        <div className={s.container}>
          <span
            className={cx(
              s.navTab,
              currentLocation === '/' ? s.linkActive : '',
            )}
          >
            <Link className={s.link} to="/">
              Dashboard
            </Link>
          </span>
          <span
            className={cx(
              s.navTab,
              currentLocation === '/allLabs' ? s.linkActive : '',
            )}
          >
            <Link className={s.link} to="/allLabs">
              All Labs
            </Link>
          </span>
          <span
            className={cx(
              s.navTab,
              currentLocation === '/addNew' ? s.linkActive : '',
            )}
          >
            <Link className={s.link} to="/addNew">
              Add New Lab
            </Link>
          </span>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(NavTabs);
