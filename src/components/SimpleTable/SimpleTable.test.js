/* eslint-disable no-undef */
import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { SimpleTable } from './SimpleTable';

Enzyme.configure({ adapter: new Adapter() });

const rows = [
  {
    labName: 'testtest111',
    labDate: '11/25/2018',
    labValue: '2.5',
    id: '2654654564',
  },
  {
    labName: 'testtest222',
    labDate: '11/15/2018',
    labValue: '5.5',
    id: '2654879564',
  },
];

function handelUpdates(obj) {
  // eslint-disable-next-line no-console
  console.log(obj);
}
describe('SimpleTable', () => {
  it('should have an 3 table rows 1 for the header and 2 for the data', () => {
    const wrapper = shallow(
      <SimpleTable rows={rows} handelUpdates={handelUpdates} showActions />,
    );

    // console.log(wrapper.debug());
    expect(wrapper.find('WithStyles(TableRow)').length).toBe(3);
  });
});
