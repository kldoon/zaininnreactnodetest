/* eslint-disable react/jsx-filename-extension */
import React from 'react';
// eslint-disable-next-line no-unused-vars
import PropTypes, { func } from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
// eslint-disable-next-line import/no-named-as-default
import EditLab from '../EditLab/EditLab';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

export function SimpleTable(props) {
  return (
    <Paper>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Lab Name</TableCell>
            <TableCell>Lab Value</TableCell>
            <TableCell>Lab Date</TableCell>
            {props.showActions ? <TableCell>Actions</TableCell> : null}
          </TableRow>
        </TableHead>
        <TableBody>
          {props.rows.map(row => (
            <TableRow key={row.id}>
              <TableCell>{row.labName}</TableCell>
              <TableCell>{row.labValue}</TableCell>
              <TableCell>
                {new Intl.DateTimeFormat('en-US').format(new Date(row.labDate))}
              </TableCell>
              {props.showActions ? (
                <TableCell>
                  <EditLab lab={row} handelUpdates={props.handelUpdates} />
                </TableCell>
              ) : null}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
}

SimpleTable.propTypes = {
  rows: PropTypes.arrayOf(
    PropTypes.shape({
      labName: PropTypes.string.isRequired,
      labValue: PropTypes.string.isRequired,
      labDate: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
    }),
  ).isRequired,
  handelUpdates: PropTypes.func.isRequired,
  showActions: PropTypes.bool.isRequired,
};

export default withStyles(styles)(SimpleTable);
