/* eslint-disable no-undef */
// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

// Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../src/server');

chai.use(chaiHttp);

/*
  * Test the /GET route
  */
describe('/GET recentlabs', () => {
  it('it should GET the most reacent lab for each unique Lab Name', done => {
    chai
      .request(server)
      .get('/api/recentlabs')
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
});
